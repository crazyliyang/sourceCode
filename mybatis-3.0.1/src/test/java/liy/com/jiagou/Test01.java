package liy.com.jiagou;

import liy.com.jiagou.mybatis30.entity.SysRoleEntity;
import liy.com.jiagou.mybatis30.mapper.RoleMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;


public class Test01 {


    @Test
    public void test01() throws IOException {

        String resource = "mybatis-config.xml";

        InputStream inputStream = Resources.getResourceAsStream(resource);

        Reader readers = new InputStreamReader(inputStream);


        // 初始化 SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(readers);

        SqlSession sqlSession = sqlSessionFactory.openSession();


        RoleMapper roleMapper = sqlSession.getMapper(RoleMapper.class);

        String id = "101";  //

        //SysRoleEntity selectByPrimaryKey(String id);
        SysRoleEntity role = roleMapper.selectByPrimaryKey(id);

        System.err.println(  role.toString() );
    }

}
