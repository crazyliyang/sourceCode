package liy.com.jiagou.mybatis30;

import liy.com.jiagou.mybatis30.entity.SysRoleEntity;
import liy.com.jiagou.mybatis30.mapper.RoleMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;


/**
 *
 *  基于 mybatis-config.xml配置文件   和  Mapper.xml 文件
 *
 */
public class MianTestMybtis {

    public static void main(String[] args) throws IOException {

        String resource = "mybatis-config.xml";

        InputStream inputStream = Resources.getResourceAsStream(resource);

        Reader readers = new InputStreamReader(inputStream);


        // 初始化 SqlSessionFactory
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(readers);

        SqlSession sqlSession = sqlSessionFactory.openSession();


        Configuration configuration = sqlSession.getConfiguration();


        RoleMapper roleMapper = sqlSession.getMapper(RoleMapper.class);

       // String id = "3c5ae2ea024d4a63961c5135f5c977ce";  // 值班站长
        String id = "101";  //

        SysRoleEntity role = roleMapper.selectByPrimaryKey(id);

        System.err.println(  role.toString() );



    }

}
