package org.apache.ibatis.executor.result;

import org.apache.ibatis.session.ResultContext;

public class DefaultResultContext implements ResultContext {


  private Object resultObject; // 结果对象

  private int resultCount;  // 结果集  一般查询 只有一个结果集 ResultSet;

  private boolean stopped;

  public DefaultResultContext() {
    resultObject = null;
    resultCount = 0;
    stopped = false;
  }

  public Object getResultObject() {
    return resultObject;
  }

  public int getResultCount() {
    return resultCount;
  }

  public boolean isStopped() {
    return stopped;
  }

  public void nextResultObject(Object resultObject) {
    resultCount++;
    this.resultObject = resultObject;
  }

  public void stop() {
    this.stopped = true;
  }

}
