/*
 * Copyright 2002-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.context.support;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.support.MergedBeanDefinitionPostProcessor;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.core.OrderComparator;
import org.springframework.core.Ordered;
import org.springframework.core.PriorityOrdered;
import org.springframework.lang.Nullable;

/**
 *  Delegate for AbstractApplicationContext's post-processor handling.
 *
 *  delegate:  Delegate    n, v. 授（权），把……委托给他人；委派……为代表，任命
 *
 *
 *  Spring 委托机制 设计原则: 委托,委派,
 *
 *  Spring (委派) 本类, 调用 每一个实现了 BeanDefinitionRegistryPostProcessor , BeanFactoryPostProcessor  接口的类去处理事情
 *
 *  BeanDefinitionRegistryPostProcessor 是 BeanFactoryPostProcessor  的子类
 *
 *

 */
final class PostProcessorRegistrationDelegate {

	public static void invokeBeanFactoryPostProcessors(
			ConfigurableListableBeanFactory beanFactory,   // 工厂 DefaultListableBeanFactory  是 ConfigurableListableBeanFactory 接口的唯一实现类
			//自定义BeanFactoryPostProcessor
			List<BeanFactoryPostProcessor> beanFactoryPostProcessors ) {


		// Invoke BeanDefinitionRegistryPostProcessors first, if any.
		// 调用 BeanDefinitionRegistryPostProcessors
		// 所有 processed bean names
		Set<String> processedBeans = new HashSet<>();


		if (beanFactory instanceof BeanDefinitionRegistry) {   // 如果 beanFactory 有实现 BeanDefinitionRegistry

			BeanDefinitionRegistry registry = (BeanDefinitionRegistry) beanFactory;


			/**
			*   用户 BeanFactoryPostProcessor
			 *   regular:  adj. 普通的；整齐的；
			**/
			List<BeanFactoryPostProcessor> regularPostProcessors = new ArrayList<>();



			/**
			 *  用户 BeanDefinitionRegistryPostProcessor
			 *
			 *  BeanDefinitionRegistryPostProcessor 是 BeanFactoryPostProcessor  的子类
			 */
			List<BeanDefinitionRegistryPostProcessor> registryProcessors = new ArrayList<>();



			/**
			 *
			 *   说白了就是把  BeanFactoryPostProcessor  和 BeanDefinitionRegistryPostProcessor 分开处理
			 *
			 *
			 *   因为 实现了父类, 不一定实现了 子类 ,  所有要分开处理
			 *
			 */


			for (BeanFactoryPostProcessor postProcessor : beanFactoryPostProcessors) {

				if (postProcessor instanceof BeanDefinitionRegistryPostProcessor) {

					BeanDefinitionRegistryPostProcessor registryProcessor = (BeanDefinitionRegistryPostProcessor) postProcessor;

					registryProcessor.postProcessBeanDefinitionRegistry(registry);

					// 用户的 BeanDefinitionRegistryPostProcessor List
					registryProcessors.add(registryProcessor);
				}

				else {

					// 用户  BeanFactoryPostProcessor
					regularPostProcessors.add(postProcessor);

				}
			}


			/**
			 *   Spring 内部的 BeanDefinitionRegistryPostProcessor
			 *
			 */
			List<BeanDefinitionRegistryPostProcessor> currentRegistryProcessors = new ArrayList<>();


			/**
			 *    从工厂中, 能通过类型得到 beanNames;
			 *    从工厂中 , 能通 BeanDefinitionRegistryPostProcessor.class 类型  得到所有 beanNames;
			 *    通过 Bean 的类型Class 得到 beanName
			 */
			String[] postProcessorNames =

					beanFactory.getBeanNamesForType( BeanDefinitionRegistryPostProcessor.class, true, false);


			// 循环 beanNames
			for (String ppName : postProcessorNames) {

				if (beanFactory.isTypeMatch(ppName, PriorityOrdered.class)) {


					// Spring 内部的 BeanDefinitionRegistryPostProcessor List
					currentRegistryProcessors.add( beanFactory.getBean( ppName, BeanDefinitionRegistryPostProcessor.class));


					processedBeans.add(ppName);
				}
			}


			// Spring 内部的 BeanDefinitionRegistryPostProcessor List
			// 排序
			sortPostProcessors(currentRegistryProcessors, beanFactory);


			// 用户 BeanDefinitionRegistryPostProcessor List

			// 合并 Spring 内部的 BeanDefinitionRegistryPostProcessor List
			registryProcessors.addAll(  currentRegistryProcessors  );



			/**
			    执行: invoke  Bean-Definition-Registry-Post-Processors
			    Spring 内部的 BeanDefinitionRegistryPostProcessor List
			    调用方法 处理 BeanDefinitionRegistryPostProcessor
			    如果用户没有  自定义BeanDefinitionRegistryPostProcessor , 那么就只有一条


			    就是: Class:   ConfigurationClassPostProcessor implements BeanDefinitionRegistryPostProcessor,

			         beanName:   org.springframework.context.annotation.internalConfigurationAnnotationProcessor

			    目前就只有这一条

			*/
			invokeBeanDefinitionRegistryPostProcessors(currentRegistryProcessors, registry);



			// Spring 内部的 BeanDefinitionRegistryPostProcessor List
			// 清理
			currentRegistryProcessors.clear();



			// Next, invoke the BeanDefinitionRegistryPostProcessors that implement Ordered.
			postProcessorNames = beanFactory.getBeanNamesForType(BeanDefinitionRegistryPostProcessor.class, true, false);


			for (String ppName : postProcessorNames) {
				if (!processedBeans.contains(ppName) && beanFactory.isTypeMatch(ppName, Ordered.class)) {

					// Spring 内部的 BeanDefinitionRegistryPostProcessor List
					currentRegistryProcessors.add(beanFactory.getBean(ppName, BeanDefinitionRegistryPostProcessor.class));
					processedBeans.add(ppName);
				}
			}


			// Spring 内部的 BeanDefinitionRegistryPostProcessor List
			sortPostProcessors(currentRegistryProcessors, beanFactory);

			// 用户的 BeanDefinitionRegistryPostProcessor List
			registryProcessors.addAll(currentRegistryProcessors);


			// Spring 内部的 BeanDefinitionRegistryPostProcessor List
			invokeBeanDefinitionRegistryPostProcessors(currentRegistryProcessors, registry);

			// Spring 内部的 BeanDefinitionRegistryPostProcessor List
			currentRegistryProcessors.clear();


			// Finally, invoke all other BeanDefinitionRegistryPostProcessors until no further ones appear.
			boolean reiterate = true;



			while (reiterate) {
				reiterate = false;
				postProcessorNames = beanFactory.getBeanNamesForType(BeanDefinitionRegistryPostProcessor.class, true, false);
				for (String ppName : postProcessorNames) {
					if (!processedBeans.contains(ppName)) {

						// Spring 内部的 BeanDefinitionRegistryPostProcessor List
						currentRegistryProcessors.add(beanFactory.getBean(ppName, BeanDefinitionRegistryPostProcessor.class));

						processedBeans.add(ppName);
						reiterate = true;
					}
				}

				// Spring 内部的 BeanDefinitionRegistryPostProcessor List
				sortPostProcessors(currentRegistryProcessors, beanFactory);


				// 用户的 BeanDefinitionRegistryPostProcessor List
				registryProcessors.addAll(currentRegistryProcessors);


				// Spring 内部的 BeanDefinitionRegistryPostProcessor List
				invokeBeanDefinitionRegistryPostProcessors(currentRegistryProcessors, registry);

				// Spring 内部的 BeanDefinitionRegistryPostProcessor List
				currentRegistryProcessors.clear();
			}


			// Now, invoke the postProcessBeanFactory callback of all processors handled so far.
			// 用户的 BeanDefinitionRegistryPostProcessor List
			/**
			 * 这里是 处理 父类 BeanFactoryPostProcessor
			 *
			 * interface  BeanDefinitionRegistryPostProcessor  extends BeanFactoryPostProcessor
			 *
			 * 因为 子类接口的方法都处理完毕了,  现在需要处理 父类接口的方法了
			 *
			 *
			 */
			invokeBeanFactoryPostProcessors(registryProcessors, beanFactory);  //Registry



			// 用户  BeanFactoryPostProcessor
			/**
			 * interface  BeanDefinitionRegistryPostProcessor  extends BeanFactoryPostProcessor
			 *
			 * 因为 子类BeanDefinitionRegistryPostProcessor接口的方法都处理完毕了,
			 *
			 * 现在需要处理, 父类BeanFactoryPostProcessor 接口的方法了
			 *
			 */
			invokeBeanFactoryPostProcessors(regularPostProcessors, beanFactory);  //
		}




		else {
			// Invoke factory processors registered with the context instance.
			invokeBeanFactoryPostProcessors(beanFactoryPostProcessors, beanFactory);
		}

		// Do not initialize FactoryBeans here: We need to leave all regular beans
		// uninitialized to let the bean factory post-processors apply to them!
		String[] postProcessorNames =
				beanFactory.getBeanNamesForType(BeanFactoryPostProcessor.class, true, false);

		// Separate between BeanFactoryPostProcessors that implement PriorityOrdered,
		// Ordered, and the rest.
		List<BeanFactoryPostProcessor> priorityOrderedPostProcessors = new ArrayList<>();
		List<String> orderedPostProcessorNames = new ArrayList<>();
		List<String> nonOrderedPostProcessorNames = new ArrayList<>();
		for (String ppName : postProcessorNames) {
			if (processedBeans.contains(ppName)) {
				// skip - already processed in first phase above
			}
			else if (beanFactory.isTypeMatch(ppName, PriorityOrdered.class)) {
				priorityOrderedPostProcessors.add(beanFactory.getBean(ppName, BeanFactoryPostProcessor.class));
			}
			else if (beanFactory.isTypeMatch(ppName, Ordered.class)) {
				orderedPostProcessorNames.add(ppName);
			}
			else {
				nonOrderedPostProcessorNames.add(ppName);
			}
		}

		// First, invoke the BeanFactoryPostProcessors that implement PriorityOrdered.
		sortPostProcessors(priorityOrderedPostProcessors, beanFactory);
		invokeBeanFactoryPostProcessors(priorityOrderedPostProcessors, beanFactory);

		// Next, invoke the BeanFactoryPostProcessors that implement Ordered.
		List<BeanFactoryPostProcessor> orderedPostProcessors = new ArrayList<>();
		for (String postProcessorName : orderedPostProcessorNames) {
			orderedPostProcessors.add(beanFactory.getBean(postProcessorName, BeanFactoryPostProcessor.class));
		}
		sortPostProcessors(orderedPostProcessors, beanFactory);


		invokeBeanFactoryPostProcessors(orderedPostProcessors, beanFactory);



		// Finally, invoke all other BeanFactoryPostProcessors.
		List<BeanFactoryPostProcessor> nonOrderedPostProcessors = new ArrayList<>();
		for (String postProcessorName : nonOrderedPostProcessorNames) {
			nonOrderedPostProcessors.add(beanFactory.getBean(postProcessorName, BeanFactoryPostProcessor.class));
		}



		invokeBeanFactoryPostProcessors(nonOrderedPostProcessors, beanFactory);


		// Clear cached merged bean definitions since the post-processors might have
		// modified the original metadata, e.g. replacing placeholders in values...
		beanFactory.clearMetadataCache();

	}

	public static void registerBeanPostProcessors(
			ConfigurableListableBeanFactory beanFactory, AbstractApplicationContext applicationContext) {

		String[] postProcessorNames = beanFactory.getBeanNamesForType(BeanPostProcessor.class, true, false);

		// Register BeanPostProcessorChecker that logs an info message when
		// a bean is created during BeanPostProcessor instantiation, i.e. when
		// a bean is not eligible for getting processed by all BeanPostProcessors.
		int beanProcessorTargetCount = beanFactory.getBeanPostProcessorCount() + 1 + postProcessorNames.length;
		beanFactory.addBeanPostProcessor(new BeanPostProcessorChecker(beanFactory, beanProcessorTargetCount));

		// Separate between BeanPostProcessors that implement PriorityOrdered,
		// Ordered, and the rest.
		List<BeanPostProcessor> priorityOrderedPostProcessors = new ArrayList<>();
		List<BeanPostProcessor> internalPostProcessors = new ArrayList<>();
		List<String> orderedPostProcessorNames = new ArrayList<>();
		List<String> nonOrderedPostProcessorNames = new ArrayList<>();
		for (String ppName : postProcessorNames) {
			if (beanFactory.isTypeMatch(ppName, PriorityOrdered.class)) {
				BeanPostProcessor pp = beanFactory.getBean(ppName, BeanPostProcessor.class);
				priorityOrderedPostProcessors.add(pp);
				if (pp instanceof MergedBeanDefinitionPostProcessor) {
					internalPostProcessors.add(pp);
				}
			}
			else if (beanFactory.isTypeMatch(ppName, Ordered.class)) {
				orderedPostProcessorNames.add(ppName);
			}
			else {
				nonOrderedPostProcessorNames.add(ppName);
			}
		}

		// First, register the BeanPostProcessors that implement PriorityOrdered.
		sortPostProcessors(priorityOrderedPostProcessors, beanFactory);
		registerBeanPostProcessors(beanFactory, priorityOrderedPostProcessors);

		// Next, register the BeanPostProcessors that implement Ordered.
		List<BeanPostProcessor> orderedPostProcessors = new ArrayList<>();
		for (String ppName : orderedPostProcessorNames) {
			BeanPostProcessor pp = beanFactory.getBean(ppName, BeanPostProcessor.class);
			orderedPostProcessors.add(pp);
			if (pp instanceof MergedBeanDefinitionPostProcessor) {
				internalPostProcessors.add(pp);
			}
		}
		sortPostProcessors(orderedPostProcessors, beanFactory);
		registerBeanPostProcessors(beanFactory, orderedPostProcessors);

		// Now, register all regular BeanPostProcessors.
		List<BeanPostProcessor> nonOrderedPostProcessors = new ArrayList<>();
		for (String ppName : nonOrderedPostProcessorNames) {
			BeanPostProcessor pp = beanFactory.getBean(ppName, BeanPostProcessor.class);
			nonOrderedPostProcessors.add(pp);
			if (pp instanceof MergedBeanDefinitionPostProcessor) {
				internalPostProcessors.add(pp);
			}
		}
		registerBeanPostProcessors(beanFactory, nonOrderedPostProcessors);

		// Finally, re-register all internal BeanPostProcessors.
		sortPostProcessors(internalPostProcessors, beanFactory);
		registerBeanPostProcessors(beanFactory, internalPostProcessors);

		// Re-register post-processor for detecting inner beans as ApplicationListeners,
		// moving it to the end of the processor chain (for picking up proxies etc).
		beanFactory.addBeanPostProcessor(new ApplicationListenerDetector(applicationContext));
	}

	private static void sortPostProcessors(List<?> postProcessors, ConfigurableListableBeanFactory beanFactory) {
		Comparator<Object> comparatorToUse = null;
		if (beanFactory instanceof DefaultListableBeanFactory) {
			comparatorToUse = ((DefaultListableBeanFactory) beanFactory).getDependencyComparator();
		}
		if (comparatorToUse == null) {
			comparatorToUse = OrderComparator.INSTANCE;
		}
		postProcessors.sort(comparatorToUse);
	}

	/**
	 *  Invoke the given BeanDefinitionRegistryPostProcessor beans.
	 *
	 *  调用 处理 BeanDefinitionRegistryPostProcessors
	 *
	 *  遍历调用每一个 BeanDefinitionRegistryPostProcessors
	 *
	 */
	private static void invokeBeanDefinitionRegistryPostProcessors(
			Collection<? extends BeanDefinitionRegistryPostProcessor> postProcessors,  // BeanDefinitionRegistryPostProcessor 集合
			// ApplicationContext
			BeanDefinitionRegistry registry ) {

		// 遍历调用每一个
		for (BeanDefinitionRegistryPostProcessor postProcessor : postProcessors) {

			// 调用  BeanDefinitionRegistryPostProcessor 接口实现类的 postProcessBeanDefinitionRegistry() 方法
			postProcessor.postProcessBeanDefinitionRegistry(registry);

		}
	}






	/**
	 * Invoke the given BeanFactoryPostProcessor beans.
	 */
	private static void invokeBeanFactoryPostProcessors(
			Collection<? extends BeanFactoryPostProcessor> postProcessors, ConfigurableListableBeanFactory beanFactory) {

		for (BeanFactoryPostProcessor postProcessor : postProcessors) {
			postProcessor.postProcessBeanFactory(beanFactory);
		}
	}

	/**
	 * Register the given BeanPostProcessor beans.
	 */
	private static void registerBeanPostProcessors(
			ConfigurableListableBeanFactory beanFactory, List<BeanPostProcessor> postProcessors) {

		for (BeanPostProcessor postProcessor : postProcessors) {
			beanFactory.addBeanPostProcessor(postProcessor);
		}
	}


	/**
	 * BeanPostProcessor that logs an info message when a bean is created during
	 * BeanPostProcessor instantiation, i.e. when a bean is not eligible for
	 * getting processed by all BeanPostProcessors.
	 */
	private static final class BeanPostProcessorChecker implements BeanPostProcessor {

		private static final Log logger = LogFactory.getLog(BeanPostProcessorChecker.class);

		private final ConfigurableListableBeanFactory beanFactory;

		private final int beanPostProcessorTargetCount;

		public BeanPostProcessorChecker(ConfigurableListableBeanFactory beanFactory, int beanPostProcessorTargetCount) {
			this.beanFactory = beanFactory;
			this.beanPostProcessorTargetCount = beanPostProcessorTargetCount;
		}

		@Override
		public Object postProcessBeforeInitialization(Object bean, String beanName) {
			return bean;
		}

		@Override
		public Object postProcessAfterInitialization(Object bean, String beanName) {
			if (!(bean instanceof BeanPostProcessor) && !isInfrastructureBean(beanName) &&
					this.beanFactory.getBeanPostProcessorCount() < this.beanPostProcessorTargetCount) {
				if (logger.isInfoEnabled()) {
					logger.info("Bean '" + beanName + "' of type [" + bean.getClass().getName() +
							"] is not eligible for getting processed by all BeanPostProcessors " +
							"(for example: not eligible for auto-proxying)");
				}
			}
			return bean;
		}

		private boolean isInfrastructureBean(@Nullable String beanName) {
			if (beanName != null && this.beanFactory.containsBeanDefinition(beanName)) {
				BeanDefinition bd = this.beanFactory.getBeanDefinition(beanName);
				return (bd.getRole() == RootBeanDefinition.ROLE_INFRASTRUCTURE);
			}
			return false;
		}
	}

}
