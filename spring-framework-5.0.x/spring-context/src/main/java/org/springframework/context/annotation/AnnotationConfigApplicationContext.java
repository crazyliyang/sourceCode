/*
 * Copyright 2002-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.context.annotation;

import java.util.function.Supplier;

import org.springframework.beans.factory.config.BeanDefinitionCustomizer;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;

/**
 *
 *   Spring 注解配置应用上下文:  AnnotationConfigApplicationContext
 *
 *
 */
public class AnnotationConfigApplicationContext extends GenericApplicationContext implements AnnotationConfigRegistry {

	// 注解BD读取器
	private final AnnotatedBeanDefinitionReader reader;



	/**
	*  注意: 这里的 scanner  并不是负责 Spring 上下文初始化时 扫描类 成为BD
	 *
	 * 而  AnnotationConfigApplicationContext.scan( basePackages )  时起作用
	 *
	 * 外部添加 scan 时起作用的
	 *
	*/
	private final ClassPathBeanDefinitionScanner scanner;


	/**
	 *
	 *  先实例化其父类的 构造
	 *
	 */
	public AnnotationConfigApplicationContext() {

		/**
		   Annotated adj. 有注释的；带注解的;

		   只能读取加载, 加了注解类 -> BD

		   将自己 AnnotationConfigApplicationContext 传给  AnnotatedBeanDefinitionReader 的构造
		   因为 AnnotationConfigApplicationContext 通过继承也间接实现了 BeanDefinitionRegistry
		   AnnotationConfigApplicationContext 本身就是一个 注册器 BDR
		*/
		this.reader = new AnnotatedBeanDefinitionReader(this);



		// 能过扫描 ClassPath下的类 -> 变成BD
		// 扫描包下的类 变成 bd 存放到 beanFactory 的 bd_map
		this.scanner = new ClassPathBeanDefinitionScanner(this);
	}



	/**
	 * 构造 DefaultListableBeanFactory
	 */
	public AnnotationConfigApplicationContext(DefaultListableBeanFactory beanFactory) {

		super(beanFactory);

		this.reader = new AnnotatedBeanDefinitionReader(this);
		this.scanner = new ClassPathBeanDefinitionScanner(this);
	}



	/**
	 *  构造 Class[]
	 */
	public AnnotationConfigApplicationContext(Class<?>... annotatedClasses) {
		this();
		register(annotatedClasses);
		refresh();
	}

	/**
	 *  构造 basePackages
	 */
	public AnnotationConfigApplicationContext(String... basePackages) {
		this();
		scan(basePackages);
		refresh();
	}


	/**
	 *
	 */
	@Override
	public void setEnvironment(ConfigurableEnvironment environment) {
		super.setEnvironment(environment);
		this.reader.setEnvironment(environment);
		this.scanner.setEnvironment(environment);
	}

	/**

	 */
	public void setBeanNameGenerator(BeanNameGenerator beanNameGenerator) {
		this.reader.setBeanNameGenerator(beanNameGenerator);
		this.scanner.setBeanNameGenerator(beanNameGenerator);
		getBeanFactory().registerSingleton(
				AnnotationConfigUtils.CONFIGURATION_BEAN_NAME_GENERATOR, beanNameGenerator);
	}

	/**
	 *
	 */
	public void setScopeMetadataResolver(ScopeMetadataResolver scopeMetadataResolver) {
		this.reader.setScopeMetadataResolver(scopeMetadataResolver);
		this.scanner.setScopeMetadataResolver(scopeMetadataResolver);
	}


	//---------------------------------------------------------------------
	// Implementation of AnnotationConfigRegistry
	//---------------------------------------------------------------------


	/**
	 *
	 *  注册 bd
	 *
	 */
	public void register(Class<?>... annotatedClasses) {

		Assert.notEmpty(annotatedClasses, "At least one annotated class must be specified");

		// 调用 读取器的(AnnotatedBeanDefinitionReader)  register
		this.reader.register(annotatedClasses);
	}

	/**

	 */
	public void scan(String... basePackages) {
		Assert.notEmpty(basePackages, "At least one base package must be specified");
		this.scanner.scan(basePackages);
	}


	//---------------------------------------------------------------------
	// Convenient methods for registering individual beans
	//---------------------------------------------------------------------

	/**
	 *
	 * @since 5.0
	 */
	public <T> void registerBean(Class<T> annotatedClass, Object... constructorArguments) {
		registerBean(null, annotatedClass, constructorArguments);
	}

	/**
	 *
	 * @since 5.0
	 */
	public <T> void registerBean(@Nullable String beanName, Class<T> annotatedClass, Object... constructorArguments) {
		this.reader.doRegisterBean(annotatedClass, null, beanName, null,
				bd -> {
					for (Object arg : constructorArguments) {
						bd.getConstructorArgumentValues().addGenericArgumentValue(arg);
					}
				});
	}

	@Override
	public <T> void registerBean(@Nullable String beanName, Class<T> beanClass, @Nullable Supplier<T> supplier,
			BeanDefinitionCustomizer... customizers) {

		this.reader.doRegisterBean(beanClass, supplier, beanName, null, customizers);
	}

}
