package com.liy.components;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;

@Service
@ComponentScan("com.liy")
public class ServiceImpl {

	public ServiceImpl() {
		System.out.println( "ServiceImpl Construct 构造" );
	}

	public void service(){
		System.out.println("ServiceImpl service() ....");
	}
}
