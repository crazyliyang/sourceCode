package com.liy;

import com.liy.components.ServiceImpl;
import com.liy.config.AppConfig;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainLuBan24 {

	public static void main(String[] args) {

		/**
		 * 如果 加入了 自动扫包的 配置  @ComponentScan("com.liy") AppConfig
		 *
		 * 自动扫描注册Bean, 则无需手动 context.register 注册Bean,
		 *
		 * 否则报错: GenericApplicationContext does not support multiple refresh attempts: just call 'refresh' once
		 *
		 *  attempt: n. 企图，试图；攻击
		 * 			 vt. 企图，试图；尝试
 		 */
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();


		/**
		 * 手动添加 BeanFactoryPostProcessor
		 * 注意手动添加需要调用: context.refresh() 才会生效
 		 */
		context.addBeanFactoryPostProcessor( new BeanFactoryPostProcessor() {
			@Override
			public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
				System.out.println(" 手动添加的 BeanFactoryPostProcessor,  通过: AbstractApplicationContext # addBeanFactoryPostProcessor() ");
			}
		});


		context.register(ServiceImpl.class);

		context.refresh();

		ServiceImpl service = context.getBean(ServiceImpl.class);

		service.service();
	}
}
