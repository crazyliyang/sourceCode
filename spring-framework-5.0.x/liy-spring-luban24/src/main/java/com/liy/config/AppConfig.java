package com.liy.config;


import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.liy")
public class AppConfig {
}
