package com.liy.beanFactoryPostProcessor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

/**
 * 在Spring初始化Bean之前,  可以改变 BD , 从而改变Bean的初始化结果
 *
 * 这种带 @Component 的BeanFactoryPostProcessor实现类 可以由Spring 自动扫描注册到容器中
 *
 */
@Component
public class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {


	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

		System.out.println(" MyBeanFactoryPostProcessor  postProcessBeanFactory()  invoked  ");

	}
}
