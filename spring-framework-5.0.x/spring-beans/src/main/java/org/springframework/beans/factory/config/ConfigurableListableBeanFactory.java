/*
 * Copyright 2002-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.beans.factory.config;

import java.util.Iterator;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.lang.Nullable;

/**
 *
 *  ConfigurableListableBeanFactory 的唯一实现类: DefaultListableBeanFactory
 *
 * @see //org.springframework.context.support.AbstractApplicationContext#getBeanFactory()
 */
public interface ConfigurableListableBeanFactory extends ListableBeanFactory, AutowireCapableBeanFactory, ConfigurableBeanFactory {


	/**
	 *  获取 bd
	 */
	BeanDefinition getBeanDefinition(String beanName) throws NoSuchBeanDefinitionException;

	/**
	 * 获取 beanName list
	 */
	Iterator<String> getBeanNamesIterator();


	/**
	 *
	 */
	void ignoreDependencyType(Class<?> type);


	/**
	 *
	 */
	void ignoreDependencyInterface(Class<?> ifc);

	/**
	  *
	  *
	  *
	  */
	void registerResolvableDependency(Class<?> dependencyType, @Nullable Object autowiredValue);

	/**

	 */
	boolean isAutowireCandidate(String beanName, DependencyDescriptor descriptor) throws NoSuchBeanDefinitionException;



	/**
	 *
	 *
	 */
	void clearMetadataCache();

	/**
	 *
	 */
	void freezeConfiguration();

	/**

	 */
	boolean isConfigurationFrozen();

	/**
	 * Ensure that all non-lazy-init singletons are instantiated, also considering
	 */
	void preInstantiateSingletons() throws BeansException;

}
