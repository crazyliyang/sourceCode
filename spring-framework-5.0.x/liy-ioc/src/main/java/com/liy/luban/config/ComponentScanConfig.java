package com.liy.luban.config;


import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.liy.luban")
public class ComponentScanConfig {
}
