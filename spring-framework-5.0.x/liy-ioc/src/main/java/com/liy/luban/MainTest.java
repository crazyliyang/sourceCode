package com.liy.luban;

import com.liy.luban.components.ServiceImpl;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainTest {

	public static void main(String[] args) {


		/**
		 * 写法一:
		 */
		/*
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ComponentScanConfig.class);
		DaoImpl dao = context.getBean(DaoImpl.class);
		dao.add();
		*/


		/**
		 * 写法二:
		 */

		AnnotationConfigApplicationContext context22 = new AnnotationConfigApplicationContext();
		/*
		 * 向 ApplicationContext 注册 单个Bean,  只要是一个 Bean的Class即可
		 */
		context22.register(ServiceImpl.class);



		/**
		 * 必须要刷新 ApplicationContext 否则报错:
		 * java.lang.IllegalStateException: org.springframework.context.annotation.AnnotationConfigApplicationContext # has not been refreshed yet
		 */
		context22.refresh();

		ServiceImpl service = context22.getBean(ServiceImpl.class);
		service.service();
	}
}
