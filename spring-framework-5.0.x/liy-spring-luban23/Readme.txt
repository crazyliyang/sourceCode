Spring的核心:  BeanPostProcessor  Spring 的后置处理器

1. BeanPostProcessor 接口:   是Spring 提供的一个扩展Bean初始化的点 (不止这一个)

      # postProcessBeforeInitialization(Object bean, String beanName)
      # postProcessAfterInitialization(Object bean, String beanName)


2. PriorityOrdered 接口:  init getOrder() 返回值决定了 后置处理器的执行顺序,值越小越先执行  ( 1 > 2 > 3 )

      # init getOrder()