package com.liy.luban;

import com.liy.luban.components.ServiceImpl;
import com.liy.luban.config.AppConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class LuBan23Main {

	public static void main(String[] args) {

		/**
		 * 如果 加入了 自动扫包的 配置  @ComponentScan("com.liy.luban") AppConfig
		 *
		 * 自动扫描注册Bean, 则无需手动 context.register 注册Bean,
		 *
		 * 否则报错: GenericApplicationContext does not support multiple refresh attempts: just call 'refresh' once
		 *
		 *  attempt: n. 企图，试图；攻击
		 * 			 vt. 企图，试图；尝试
 		 */
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();

		// 注册Bean, 可以是任意的Bean: 包括配置类
		context.register(ServiceImpl.class);

		/**
		 * 手动注册Bean, 必须手动刷新 refresh()
		 * 否则报错: AnnotationConfigApplicationContext, has not been refreshed yet
		 */
		context.refresh();

		ServiceImpl service = context.getBean(ServiceImpl.class);

		service.service();
	}
}
