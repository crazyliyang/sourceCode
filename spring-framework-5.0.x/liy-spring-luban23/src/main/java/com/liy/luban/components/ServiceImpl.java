package com.liy.luban.components;


import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;

// @Service
@ComponentScan("com.liy.luban")
public class ServiceImpl {

	public ServiceImpl() {
		System.out.println( " Construct 构造" );
	}

	public void service(){
		System.out.println("LuBan-23 ServiceImpl service() ....");
	}
}
