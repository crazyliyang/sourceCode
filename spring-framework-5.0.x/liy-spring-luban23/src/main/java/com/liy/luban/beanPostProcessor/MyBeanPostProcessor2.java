package com.liy.luban.beanPostProcessor;


import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.PriorityOrdered;
import org.springframework.stereotype.Component;

/**
 *
 * 自定义 Spring 后置处理器
 *
 */
@Component
public class MyBeanPostProcessor2 implements BeanPostProcessor, PriorityOrdered {


	/**
	 * Before Initialization
	 *
	 * @param bean:
	 * @param beanName:
	 */
	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {

		// System.out.println( "所有的 beanName -> " + beanName );

		if(beanName.equals("serviceImpl")){
			System.out.println(" MyBeanPostProcessor2  postProcess - Before - Initialization  ");
		}

		return bean;
	}


	/**
	 * After Initialization
	 *
	 */
	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if(beanName.equals("serviceImpl")){
			System.out.println("MyBeanPostProcessor2 postProcess - After - Initialization  ");
		}
		return bean;
	}

	@Override
	public int getOrder() {
		return 100;
	}
}
