package com.liy.luban.beanPostProcessor;


import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.PriorityOrdered;
import org.springframework.stereotype.Component;

/**
 *
 * BeanPostProcessor 接口: Spring 后置处理器
 *
 *  PriorityOrdered 接口:  init getOrder() 返回值决定了 后置处理器的执行顺序,值越小越先执行  ( 1 > 2 > 3 )
 *
 */
@Component
public class MyBeanPostProcessor implements BeanPostProcessor, PriorityOrdered {


	/**
	 * Before Initialization
	 *
	 * @param bean:
	 * @param beanName:
	 */
	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {

		//System.out.println( "所有的 beanName -> " + beanName );

		if(beanName.equals("serviceImpl")){
			System.out.println(" postProcess - Before - Initialization  ");
		}

		return bean;
	}


	/**
	 * After Initialization
	 *
	 */
	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if(beanName.equals("serviceImpl")){
			System.out.println(" postProcess - After - Initialization  ");
		}
		return bean;
	}

	@Override
	public int getOrder() {
		return 101;
	}
}
